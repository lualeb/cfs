#=============================================================================
# Global defines for server infrastructure related to CFS development
#=============================================================================

# ----------------------------------------------------------------------------
# Mirror for cfsdeps hosted at the optimization group - plain http!
# ----------------------------------------------------------------------------
SET(CFS_FAU_MIRROR "http://movm.mi.uni-erlangen.de/cfsdepscache")

#-----------------------------------------------------------------------------
# Drop site for nightly build/test results at FAU.
# currently this is an open service at an open port
#-----------------------------------------------------------------------------
SET(CFS_DS_CDASH_DROP_SITE "neubau-157.mi.uni-erlangen.de")
